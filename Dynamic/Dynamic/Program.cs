﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Dynamic;

namespace Dynamic
{
    class Program
    {
        static void Main(string[] args)
        {
            // kindlat tüüpi muutujad
            var i = 4;  i++; 
            var s = "Henn"; s.ToUpper();

            // universaaltüüp - object
            object o;
            o = i;          Console.WriteLine(o);   
            o = s;          Console.WriteLine(o);
            o = i;          Console.WriteLine(o);
            int õ = (o as int?) ?? 0;

            // dynaamiline muutuja - tüüp selgub töö käigus
            dynamic d;
            d = i;          Console.WriteLine(d++);
            d = s;          Console.WriteLine(d.ToUpper());
            d = i;          Console.WriteLine(--d);

            Dictionary<string, dynamic> dd = new Dictionary<string, dynamic>();
            dd["nimi"] = "Keegi muu";
            dd["vanus"] = i;

            dynamic bag = new Bag();
            bag.Nimi = s;
            bag.Vanus = i;
            Console.WriteLine($"{bag.Nimi} {++bag.Vanus}");

            dynamic bag2 = new Bag(dd);
            Console.WriteLine(bag2.nimi);

            Console.WriteLine(bag2.midaeiolesedaeiole??"loll");


        }
    }
}

class Bag : DynamicObject
{
    //private Dictionary<string, dynamic> bag = new Dictionary<string, dynamic>();

    private dynamic bag;
    public Bag()
    {
        bag = new Dictionary<string, dynamic>();
    }

    public Bag(IDictionary<string,object> bag)
    {
        this.bag = bag;
    }

    public override bool TryGetMember(GetMemberBinder binder, out object result)
    {
        result = bag.ContainsKey(binder.Name) ? bag[binder.Name] : null;
        return true;
    }

    public override bool TrySetMember(SetMemberBinder binder, object value)
    {
        bag[binder.Name] = value;
        return true;
    }

}