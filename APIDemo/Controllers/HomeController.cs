﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Dynamic;
using System.Web.Routing;

namespace APIDemo.Controllers
{
    public class Bag : DynamicObject
    {
        private dynamic bag;
        public Bag(dynamic bag) => this.bag = bag;

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            try { result = bag[binder.Name]; } catch { result = null; }
            return true;
        }
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            bag[binder.Name] = value;
            return true;
        }

    }


    public class MyController : Controller
    {
        protected dynamic TempBag;
        protected dynamic SessionBag;
        protected dynamic AppBag;
        dynamic paramsBag = null;
        protected dynamic ParamsBag => paramsBag ?? new Bag(Request.Params);
        protected MyController() : base()
        {
            TempBag = new Bag(TempData);
            SessionBag = new Bag(System.Web.HttpContext.Current.Session);
            AppBag = new Bag(System.Web.HttpContext.Current.Application);
        }



        




    }


    public class HomeController : MyController
    {

        public HomeController() : base() {}

        public ActionResult Teine()
        {
            ViewBag.Teine = "Mina olen Robert";
            TempBag.Teine = "Tema on ka robert";

            return RedirectToAction("Index");
 
        }


        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            SessionBag.Nimi = ParamsBag.Nimi ?? SessionBag.Nimi;
            ViewBag.Nimi = SessionBag.Nimi;
            ViewBag.AppNr = AppBag.Nr++;
            ViewBag.SessNr = SessionBag.Nr++;

            ViewBag.Sõnum = "Tere tulemast dünaamilisse maailma";
            ViewBag.TeineSõnum = "tere tulemast sõnastike maailma";
            ViewBag.Vanaema = Request.Params["Vanaema"];

            ViewBag.QueryString = Request.QueryString;

            HttpCookie h = Request.Cookies["test"] ?? new HttpCookie("test");
            if (ParamsBag.Nimi != null)
            {
                h.Values["Nimi"] = ParamsBag.Nimi;
                h.Expires = DateTime.Now.AddHours(1);
                Response.Cookies.Add(h);
            }
            ViewBag.EndineNimi = h["Nimi"];



            return View();
        }
    }
}
