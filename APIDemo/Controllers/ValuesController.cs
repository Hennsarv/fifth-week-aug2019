﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace APIDemo.Controllers
{
    public class ValuesController : ApiController
    {
        static Dictionary<int, string> Nimed = new Dictionary<int, string>
        {
            { 1, "Henn"},
            { 2, "Ants"},
            { 3, "Peeter"},
        };

        // GET api/values
        public IEnumerable<string> Get()
        {
            //            return new string[] { "value1", "value2" };
            return Nimed.Values;
        }

        // GET api/values/5
        public string Get(int id)
        {
            //            return "value";
            return Nimed[id];
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
            Nimed.Add(
                Nimed.Keys.Max()+1,
                value
                );
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
            Nimed[id] = value;
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
            Nimed.Remove(id);
        }
    }
}
