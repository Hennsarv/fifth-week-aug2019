﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DataValidation.Models;

namespace DataValidation.Controllers
{
    public class PeopleController : Controller
    {
        private NorthwindEntities db = new NorthwindEntities();

        // GET: People
        public ActionResult Index()
        {
            return View(db.People.ToList());
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken] 
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,EMail,BirthDate,Nationality")] Person person)
        {
            // koht, kus sa saad asju kontrollida
            if ((person.FirstName ?? "").Length < 2)
                ModelState.AddModelError("FirstName", "nimi liiga lühike");
            if ((person.LastName ?? "").Length < 2)
                ModelState.AddModelError("LastName", "nimi liiga lühike");
            if ((person.EMail ?? "").Length < 2)
                ModelState.AddModelError("EMail", "meiliaadress peab olema");
            if (db.People.Where(x => x.FirstName == person.FirstName && x.LastName == person.LastName).Count() > 0)
                ModelState.AddModelError("FirstName", "selline inimene meil juba on");
            if (db.People.Where(x => x.EMail == person.EMail).Count() > 0)
                ModelState.AddModelError("EMail", "Sellise emailiga on juba keegi");
            if (person.BirthDate.AddYears(18) > DateTime.Today)
                ModelState.AddModelError("BirthDate", "inimene on liialt noorekene");

            // enne seda saad kontrolllida
            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(person);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,EMail,BirthDate,Nationality")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
